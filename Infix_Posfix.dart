import 'dart:io';
import 'dart:convert';

List token(String text) {
  //Generate method Token()
  List operater = ['+', '-', '*', '^', '/', '%', '(', ')'];
  List num = ['0', '1', '2', '3', '4', '5', '6', '7', '8', '9'];
  List result = [];
  for (var i = 0; i < text.length; i++) {
    //loop for remove space
    if (text[i] == ' ') {
      //When to index equla speace
      result.add(text[i]);
      result.remove(text[i]);
    } else {
      result.add(text[i]);
    }
  }
  result = tokenizingNegative(result);

  return result;
}

List tokenizingNegative(List tokens) {
  for (var i = 0; i < tokens.length - 1; i++) {
    for (var j = i + 1; j < tokens.length; j++) {
      if (tokens[i] == '-' && tokens[j] == '(') {
        break;
      } else if (tokens[i] == '-') {
        var temp = tokens[j];
        tokens[i] = '-$temp';
        tokens.remove(tokens[j]);
      }
    }
  }
  return tokens;
}

//Convert String to double
bool intoInt(String txt) {
  if (txt == null) {
    return false;
  } else {
    return double.tryParse(txt) != null;
  }
}

//Step infix to postfix
List infixToPosfix(List infix) {
  List operator = [];
  List posfix = [];
  infix.forEach((i) {
    if (intoInt(i)) {
      posfix.add(i);
    } else if (intoInt(i) == false && i != '(' && i != ')') {
      while (operator.isNotEmpty &&
          operator.last != '(' &&
          checkForLavel(i) < checkForLavel(operator.last)) {
        posfix.add(operator.removeLast());
      }
      operator.add(i);
    } else if (i == '(') {
      operator.add(i);
    } else if (i == ')') {
      while (operator.last != '(') {
        posfix.add(operator.removeLast());
      }
      operator.remove('(');
    }
  });
  while (operator.isEmpty == false) {
    posfix.add(operator.removeLast());
  }
  return posfix;
}

int checkForLavel(String operator) {
  if (operator == '(' || operator == ')') {
    return 4;
  }
  if (operator == '*' || operator == '/' || operator == '~/') {
    return 2;
  }
  if (operator == '%') {
    return 3;
  }
  if (operator == '+' || operator == '-') {
    return 1;
  }
  return 0;
}

double evaluatePostfix(List posfix) {
  List values = [];
  double result = 0;
  double left, right;

  posfix.forEach((i) {
    if (intoInt(i) == true) {
      double ans = double.parse(i);
      values.add(ans);
    } else {
      right = values.removeLast();
      left = values.removeLast();
      result = calculator(left, right, i);
      values.add(result);
    }
  });

  return values.first;
}

double calculator(double left, double right, String operator) {
  double result = 0;
  switch (operator) {
    case "+":
      {
        result = left + right;
      }
      break;
    case "-":
      {
        result = left - right;
      }
      break;
    case "*":
      {
        result = left * right;
      }
      break;
    case "/":
      {
        result = left / right;
      }
      break;
    case "%":
      {
        result = left % right;
      }
      break;
    case "^":
      {
        result = exponent(left, right);
      }
      break;
    default:
      {}
      break;
  }

  return result;
}

double exponent(double left, double right) {
  double expo = left;
  double result = 1;
  for (int i = 0; i < right; i++) {
    result = result * expo;
  }
  return result;
}

void main(List<String> args) {
  print('Please input your mathematicals expression:');
  String text = stdin.readLineSync() as String;
  List infix = token(text);
  List postfix = infixToPosfix(infix);
  double result = evaluatePostfix(postfix);
  print(
      '-------------------------------------------------------------------------------------');
  print('Your mathematicals expression: $text');
  print('Remove space your expression: $infix');
  print('This is your expression, convert infix to postfix: $postfix');
  print('This is your result of mathematicals expression: $result');
}
